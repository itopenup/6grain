package com.kl.sixthgrain;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiProvider {
    private static ApiProvider INSTANCE;

    private NewsApi mNewsApi;

    public static ApiProvider getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new ApiProvider();
        }

        return INSTANCE;
    }

    private ApiProvider() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://newsapi.org/v2/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        mNewsApi = retrofit.create(NewsApi.class);
    }

    public NewsApi getNewsApi() {
        return mNewsApi;
    }
}
