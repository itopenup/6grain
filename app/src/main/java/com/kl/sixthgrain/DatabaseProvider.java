package com.kl.sixthgrain;

import android.arch.persistence.db.SupportSQLiteOpenHelper;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.DatabaseConfiguration;
import android.arch.persistence.room.InvalidationTracker;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;
import android.support.annotation.NonNull;

import com.kl.sixthgrain.models.NewsDao;
import com.kl.sixthgrain.models.NewsItemModel;

@Database(entities = {NewsItemModel.class}, version = 1)
public abstract class DatabaseProvider extends RoomDatabase {

    private static DatabaseProvider INSTANCE;

    public static DatabaseProvider getInstance(Context context) {
        if (INSTANCE == null) {
            INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                    DatabaseProvider.class,
                    "sixth_grain_db")
                    .build();
        }
        return INSTANCE;
    }

    public abstract NewsDao newsDaoModel();

    @NonNull
    @Override
    protected SupportSQLiteOpenHelper createOpenHelper(DatabaseConfiguration config) {
        return null;
    }

    @NonNull
    @Override
    protected InvalidationTracker createInvalidationTracker() {
        return null;
    }

    @Override
    public void clearAllTables() {

    }
}
