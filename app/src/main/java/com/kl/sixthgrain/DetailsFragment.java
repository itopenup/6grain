package com.kl.sixthgrain;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.kl.sixthgrain.models.NewsItemModel;
import com.kl.sixthgrain.models.NewsViewModel;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DetailsFragment extends Fragment {
    private static final String ARG_ID = "id";

    private int mId;
    private NewsViewModel mNewsViewModel;

    @BindView(R.id.title)
    TextView mTitleTextView;

    @BindView(R.id.content)
    TextView mContentTextView;

    @BindView(R.id.author)
    TextView mAuthorTextView;

    @BindView(R.id.published)
    TextView mPublishedTextView;

    @BindView(R.id.url)
    TextView mUrlTextView;

    @BindView(R.id.image)
    ImageView mImageView;

    public DetailsFragment() {
        // Required empty public constructor
    }

    public static DetailsFragment newInstance(int id) {
        DetailsFragment fragment = new DetailsFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_ID, id);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mId = getArguments().getInt(ARG_ID, 0);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_details, container, false);
        ButterKnife.bind(this, view);

        mNewsViewModel = ViewModelProviders.of(getActivity()).get(NewsViewModel.class);
        mNewsViewModel.getDataItemById(mId).observe(getActivity(), new Observer<NewsItemModel>() {
            @Override
            public void onChanged(@Nullable NewsItemModel data) {
                if (data != null) {
                    mTitleTextView.setText(data.getTitle());
                    mContentTextView.setText(data.getDescription());
                    mAuthorTextView.setText(data.getAuthor());
                    mPublishedTextView.setText(data.getPublishedAt().toString());

                    mUrlTextView.setText(data.getUrl());
                    mUrlTextView.setMovementMethod(LinkMovementMethod.getInstance());

                    Picasso.get()
                            .load(data.getImageUrl())
                            .into(mImageView);
                }
            }
        });

        return view;
    }
}
