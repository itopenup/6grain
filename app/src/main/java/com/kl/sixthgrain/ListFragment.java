package com.kl.sixthgrain;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.kl.sixthgrain.models.NewsItemModel;
import com.kl.sixthgrain.models.NewsViewModel;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class ListFragment extends Fragment implements OnNewsItemClickListener {

    @BindView(R.id.list)
    RecyclerView mListView;

    private NewsViewModel mNewsViewModel;
    private NewsListAdapter mListAdapter;
    private Unbinder mUnbinder;
    private ListInteractionsListener mInteractionsListener;

    public interface ListInteractionsListener {
        void onDetailsOpen(int id);
    }

    public ListFragment() {

    }

    public static ListFragment newInstance() {
        ListFragment fragment = new ListFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list, container, false);

        mUnbinder = ButterKnife.bind(this, view);

        mListView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mListAdapter = new NewsListAdapter(this);
        mListView.setAdapter(mListAdapter);

        mNewsViewModel = ViewModelProviders.of(getActivity()).get(NewsViewModel.class);
        mNewsViewModel.getData().observe(getActivity(), new Observer<List<NewsItemModel>>() {
            @Override
            public void onChanged(@Nullable List<NewsItemModel> items) {
                mListAdapter.setItems(items);
                Toast.makeText(getActivity(), "News update", Toast.LENGTH_SHORT).show();
            }
        });
        mNewsViewModel.fetchData();

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mUnbinder.unbind();
    }

    @Override
    public void onNewsItemClickListener(NewsItemModel item) {
        if (mInteractionsListener != null) {
            mInteractionsListener.onDetailsOpen(item.id);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof ListInteractionsListener) {
            mInteractionsListener = (ListInteractionsListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement ListInteractionsListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mInteractionsListener = null;
    }
}
