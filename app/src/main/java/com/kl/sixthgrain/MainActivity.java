package com.kl.sixthgrain;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.arch.lifecycle.ViewModelProviders;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;

import com.kl.sixthgrain.models.NewsViewModel;

public class MainActivity extends AppCompatActivity implements ListFragment.ListInteractionsListener {

    private AlarmManager mAlarmManager;
    private PendingIntent mAlarmPendingIntent;
    private NewsViewModel mNewsViewModel;

    private BroadcastReceiver mNewsFetchReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            mNewsViewModel.fetchData();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.root_container, ListFragment.newInstance())
                    .commit();
        }

        mAlarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        Intent alarmIntent = new Intent(this, NewsAlarmReceiver.class);
        mAlarmPendingIntent = PendingIntent.getBroadcast(this, 0, alarmIntent, PendingIntent.FLAG_CANCEL_CURRENT);
        mAlarmManager.setInexactRepeating(AlarmManager.ELAPSED_REALTIME,
                SystemClock.elapsedRealtime(),
                5000,
                mAlarmPendingIntent);

        mNewsViewModel = ViewModelProviders.of(this).get(NewsViewModel.class);

        registerReceiver(mNewsFetchReceiver, new IntentFilter(NewsAlarmReceiver.ACTION_FETCH));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(mNewsFetchReceiver);
    }

    @Override
    public void onDetailsOpen(int id) {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.root_container, DetailsFragment.newInstance(id))
                .addToBackStack("Details")
                .commit();
    }
}
