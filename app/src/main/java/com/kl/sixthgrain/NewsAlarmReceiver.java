package com.kl.sixthgrain;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class NewsAlarmReceiver extends BroadcastReceiver {

    public static final String ACTION_FETCH = "action.fetch";

    @Override
    public void onReceive(Context context, Intent intent) {
        Intent broadcastIntent = new Intent(ACTION_FETCH);
        context.sendBroadcast(broadcastIntent);
    }
}
