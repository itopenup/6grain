package com.kl.sixthgrain;

import com.kl.sixthgrain.models.GsonNewsRootModel;
import com.kl.sixthgrain.models.NewsItemModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface NewsApi {
    @GET("top-headlines?sources=bloomberg&apiKey=34a4669f69544acfad189002cf7bb57f")
    Call<GsonNewsRootModel> newsList();
}
