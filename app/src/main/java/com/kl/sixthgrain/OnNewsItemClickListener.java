package com.kl.sixthgrain;

import com.kl.sixthgrain.models.NewsItemModel;

public interface OnNewsItemClickListener {
    void onNewsItemClickListener(NewsItemModel item);
}
