package com.kl.sixthgrain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class GsonNewsArticleModel {
    @SerializedName("title")
    @Expose
    String title;

    @SerializedName("description")
    @Expose
    String description;

    @SerializedName("urlToImage")
    @Expose
    String urlToImage;

    @SerializedName("author")
    @Expose
    String author;

    @SerializedName("publishedAt")
    @Expose
    Date publishedAt;

    @SerializedName("url")
    @Expose
    String url;
}
