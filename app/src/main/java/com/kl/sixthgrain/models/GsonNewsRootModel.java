package com.kl.sixthgrain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class GsonNewsRootModel {
    @SerializedName("status")
    @Expose
    String status;

    @SerializedName("totalResults")
    @Expose
    int totalResults;

    @SerializedName("articles")
    @Expose
    List<GsonNewsArticleModel> articles = new ArrayList<>();
}
