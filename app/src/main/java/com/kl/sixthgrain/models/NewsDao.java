package com.kl.sixthgrain.models;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

@Dao
public interface NewsDao {
    @Query("select * from NewsItemModel")
    LiveData<List<NewsItemModel>> getAllItems();

    @Query("select * from NewsItemModel where id = :id")
    LiveData<NewsItemModel> getItemById(String id);

    @Insert(onConflict = REPLACE)
    void add(NewsItemModel item);
}
