package com.kl.sixthgrain.models;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;

import java.util.Date;

@Entity
public class NewsItemModel {
    @PrimaryKey(autoGenerate = true)
    public int id;

    private String mTitle;
    private String mDescription;
    private String mImageUrl;
    private String mAuthor;
    private String mUrl;

    @TypeConverters(DateConverter.class)
    private Date mPublishedAt;

    public NewsItemModel(String title, String description, String imageUrl, String author, String url, Date publishedAt) {
        mTitle = title;
        mDescription = description;
        mImageUrl = imageUrl;
        mAuthor = author;
        mUrl = url;
        mPublishedAt = publishedAt;
    }

    public String getTitle() {
        return mTitle;
    }

    public String getDescription() {
        return mDescription;
    }

    public String getImageUrl() {
        return mImageUrl;
    }

    public String getAuthor() {
        return mAuthor;
    }

    public String getUrl() {
        return mUrl;
    }

    public Date getPublishedAt() {
        return mPublishedAt;
    }
}
