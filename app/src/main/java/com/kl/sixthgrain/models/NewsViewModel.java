package com.kl.sixthgrain.models;

import android.app.Activity;
import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;

import com.kl.sixthgrain.ApiProvider;
import com.kl.sixthgrain.DatabaseProvider;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NewsViewModel extends AndroidViewModel implements Callback<GsonNewsRootModel> {
    private LiveData<List<NewsItemModel>> mData;
    private DatabaseProvider mDbProvider;
    private ApiProvider mApiProvider;

    public NewsViewModel(Application application) {
        super(application);

        mDbProvider = DatabaseProvider.getInstance(application);
        mData = mDbProvider.newsDaoModel().getAllItems();
        mApiProvider = ApiProvider.getInstance();
    }

    public LiveData<List<NewsItemModel>> getData() {
        return mData;
    }

    public void fetchData() {
        mApiProvider.getNewsApi().newsList().enqueue(this);
    }

    public LiveData<NewsItemModel> getDataItemById(int id) {
        return mDbProvider.newsDaoModel().getItemById(String.valueOf(id));
    }

    @Override
    public void onResponse(Call<GsonNewsRootModel> call, Response<GsonNewsRootModel> response) {
        GsonNewsRootModel body = response.body();
        if (body != null && !body.articles.isEmpty()) {
            final List<GsonNewsArticleModel> articles = body.articles;

            new Thread(new Runnable() {
                @Override
                public void run() {
                    for (GsonNewsArticleModel article : articles) {
                        mDbProvider.newsDaoModel().add(new NewsItemModel(
                                article.title,
                                article.description,
                                article.urlToImage,
                                article.author,
                                article.url,
                                article.publishedAt
                        ));
                    }

                    mData = mDbProvider.newsDaoModel().getAllItems();
                }
            }).start();
        }
    }

    @Override
    public void onFailure(Call<GsonNewsRootModel> call, Throwable t) {

    }
}
